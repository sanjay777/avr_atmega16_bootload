#include <mega16.h>
#asm
   .equ __i2c_port=0x12 ;PORTD
   .equ __sda_bit=3
   .equ __scl_bit=2
#endasm
#include <i2c.h>
#include <delay.h>
#define SDI PORTC.0   //serial data input
#define LCLK PORTC.1  //latch clock
#define SCLK PORTC.2  //serial clock

// DS1307 Real Time Clock functions
#include <ds1307.h>

// Declare your global variables here
char hh,mm,ss,jam,menit,a,b;
void geser(char data, char n)
{ unsigned char hasil,coun;  
    for (coun =0 ;coun <=n; coun++)
    {
        SCLK=0;
        hasil= (data << coun) & 0x80;
        if(hasil==0x80)
        {SDI=1;}
        else
        {SDI=0;};
        SCLK=1;
        delay_us(1);
        SCLK=0;  
        delay_us(1);
    };       
 }   

char segment(char angka)
{  unsigned char a;     
                 switch (angka)
        {
                case 0: {a=0b00111111; break;}
                case 1: {a=0b00000110;break;}
                case 2: {a=0b01011011;break;}
                case 3: {a=0b01001111;break;}    
                case 4: {a=0b01100110;break;}
                case 5: {a=0b01101101;break;}   
                case 6: {a=0b01111101;break;}
                case 7: {a=0b00000111;break;}
                case 8: {a=0b01111111;break;}    
                case 9: {a=0b01101111;break;}
                //case 'A': {a=0b00000001;break;}
                //case 'B': {a=0b00000010;break;}
                //case 'C': {a=0b00000100;break;}
                //case 'D': {a=0b00001000;break;}
                //case '*': {a=0b00001001;break;}
                //case '#': {a=0b00100010;break;}
                case 'e': {a=~0b01000000;break;}
                case '0': {a=0b00000000;break;}
                //else:  {a=0b00000000;break;}
                }    
                return a;
}

void blank()
{
	char a;
	LCLK=0;
	for(a=0;a<2;a++)
	{
		geser (segment('0'),7);
	}
	LCLK=1;
}

void tampil(char a,char b,char c)
{


       PORTA=0b11111110;
       LCLK=0;
       geser(segment(a/10),7);
       geser(segment(a/10),7);
       LCLK=1;
       delay_ms(1);
       blank();
       
       PORTA=0b11111101;
       LCLK=0;
       geser(segment(a%10),7);
       geser(segment(a%10),7);
       LCLK=1;
       delay_ms(1);
       blank();
        
       PORTA=0b11111011;
       LCLK=0;
       geser(segment(b/10),7);
       geser(segment(b/10),7);
       LCLK=1;
       delay_ms(1);
       blank();
       
       PORTA=0b11110111;
       LCLK=0;
       geser(segment(b%10),7);
       geser(segment(b%10),7);
       LCLK=1;
       delay_ms(1);
       blank();
       
       PORTA=0b11101111;
       LCLK=0;
       geser(segment(c/10),7);
       geser(segment(c/10),7);
       LCLK=1;
       delay_ms(1);
       blank();
       
       PORTA=0b11011111;
       LCLK=0;
       geser(segment(c%10),7);
       geser(segment(c%10),7);
       LCLK=1;
       delay_ms(1);
       blank();
       
       PORTA=0b10111111;
       LCLK=0;
       geser(segment(0),7);
       geser(segment(0),7);
       LCLK=1;
       delay_ms(1);
       blank();
       
       
}

void padam(char a,char b,char c)
{

       PORTA=0b11111110;   
       LCLK=0;
       geser(segment(a),7);
       LCLK=1;
       delay_ms(1);
       
       PORTA=0b11111101;
       LCLK=0;
       geser(segment(a),7);
       LCLK=1;
       delay_ms(1);
        
       
       PORTA=0b11111011;
       LCLK=0;
       geser(segment(b),7);
       LCLK=1;
       delay_ms(1);
        
       
       PORTA=0b11110111;
       LCLK=0;
       geser(segment(b),7);
       LCLK=1;
       delay_ms(1);
        
       
       PORTA=0b11101111;
       LCLK=0;
       geser(segment(c),7);
       LCLK=1;
       delay_ms(1);
        
       
       PORTA=0b11011111;
       LCLK=0;
       geser(segment(c),7);
       LCLK=1;
       delay_ms(1);
       
       
       
}
void main(void)
{
	PORTA=0xFF;//switching
	DDRA=0xFF;

	PORTB=0xff;//push button
	DDRB=0b00001111;
 
	PORTC=0xFF;//data serial
	DDRC=0xFF;

	PORTD=0x00; //DS1307
	DDRD=0xFF;


// I2C Bus initialization
	i2c_init();

// DS1307 Real Time Clock initialization
// Square wave output on pin SQW/OUT: Off
// SQW/OUT pin state: 0
rtc_init(3,0,1);
//rtc_set_time(00,00,00);

awal:
PORTB.0=0;
PORTB.1=0;
while (1)
      {
      // Place your code here
      rtc_get_time(&hh,&mm,&ss);
       tampil(hh,mm,ss);
       if(PINB.5==0){goto setjam;}
       
       if ((mm==40)&&(ss==00)){goto buzz;}
      }
 
buzz:
	for(b=0;b<=10;b++)
	{
		PORTB.0=0;
		PORTB.1=0;
	for(a=0;a<=50;a++)
	{
		rtc_get_time(&hh,&mm,&ss);
		tampil(hh,mm,ss);
	}
	PORTB.0=1;
	PORTB.1=0;
	for(a=0;a<=50;a++)
	{
		rtc_get_time(&hh,&mm,&ss);
		tampil(hh,mm,ss);
	}

}
PORTB.0=0;
PORTB.1=0;
goto awal;
     
setjam:
jam=hh;
menit=mm;
while(1)
{
    for(a=0;a<=20;a++)
 {
  tampil(jam,menit,ss);
 }
 
 for(a=0;a<=20;a++)
 {
  padam('0','0','0');
 }
 
 
  if(PINB.6==0)
 {tampil(jam,menit,ss);
  jam=jam+1;if(jam==24){jam=0;}
  hh=jam;
 }
 
 if(PINB.7==0)
 {
  menit=menit+1;if(menit==60){menit=0;}
  mm=menit;
 
 }
 
 
 
 if(PINB.5==0)
 {tampil(jam,menit,ss);
  rtc_set_time(jam,menit,00);
  rtc_get_time(&hh,&mm,&ss);
  for(a=0;a<=20;a++)
 {
  tampil(hh,mm,ss);
 }
  goto awal;
 }
 }
      

}