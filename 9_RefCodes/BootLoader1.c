//https://www.engineersgarage.com/embedded/avr-microcontroller-projects/How-To-Write-a-Simple-Bootloader-For-AVR-In-C-language

#define F_CPU 8000000
#include <avr/io.h>
#include <util/delay.h>
#include <avr/boot.h>
#include <inttypes.h>
#include <avr/interrupt.h>
#include <avr/pgmspace.h>
#include <stdio.h>
#include <avr/eeprom.h>
#include "usart.h"
#include "lcd.h"
 
int main ( void )
{
uint16_t i;
uint8_t A [ 512 ];
uint8_t sreg;
uint32_t page = 0;
uint16_t w;
unsigned char *buf = A;
volatile int j = 4;
 
//====================================================================================
lcd_init(); // initialization of LCD
 
lcd_clear();
lcd_1st_line();
lcd_string("    ENGINEERS   "  );
lcd_2nd_line();
lcd_string("     GARAGE     "  );
_delay_ms(3000);
 
lcd_clear();
lcd_1st_line();
lcd_string("Booting ATMEGA16"  );
_delay_ms(2000);
 
lcd_clear();
lcd_1st_line();
lcd_string("  LCD      [OK] "  );
lcd_2nd_line();
lcd_string("   16*2, 4 bit  "  );
_delay_ms(2000);
 
usart_init(); // initialization of USART
 
lcd_clear();
lcd_1st_line();
lcd_string(" USART     [OK] "  );
lcd_2nd_line();
lcd_string("9600bps, Tx & Rx"  );
_delay_ms(2000);
 
lcd_clear();
lcd_1st_line();
lcd_string("loading App...  "  );
lcd_2nd_line();
for(i = 0; i < 16; i ++)
{
_delay_ms(350);
dis_data(0xFF);
}
 
lcd_clear();
lcd_1st_line();
//====================================================================================
 
 
//################################ BOOTING FROM EEPROM ###############################//
for ( i = 0; i < 512; i ++ )
A [ i ] = eeprom_read_byte ((const uint8_t *) i); 
 
while(1)
{
//==========================================================================//
if(j)
{
// Disable interrupts.
sreg = SREG;
cli();
eeprom_busy_wait ();
boot_page_erase (page);
boot_spm_busy_wait ();      // Wait until the memory is erased.
 
for (i=0; i<SPM_PAGESIZE; i+=2)
{
   // Set up little-endian word.
   w = *buf++;
   w += (*buf++) << 8;
 
   boot_page_fill (page + i, w);
}
boot_page_write (page);     // Store buffer in flash page.
boot_spm_busy_wait();       // Wait until the memory is written.
boot_rww_enable ();
SREG = sreg;
}
else
{
asm ( "jmp 0x0000" );
}
 
j--;
page = page + 128;
//==============================================================================//
}
//################################ BOOTING FROM EEPROM ###############################//
 
}
//#################### LCD #########################//
 
#define _LCD_H
 
#ifndef F_CPU 
#define F_CPU 8000000 
#endif 

#include<avr/io.h>
#include<util/delay.h> 
#include<inttypes.h> 
#define rs PA0 
#define rw PA1 
#define en PA2 

void lcd_init(); 
void dis_cmd(char); 
void dis_data(char); 
void lcdcmd(char); 
void lcddata(char); 
void lcd_clear(void); 
void lcd_2nd_line(void); 
void lcd_1st_line(void); 
void lcd_string(const char *data);
 
void lcd_string(const char *data) 
{ 
	for(;*data;data++) 
	dis_data (*data); 
} 

void lcd_clear(void) 
{ 
	dis_cmd(0x01); 
	_delay_ms(10); 
} 

void lcd_2nd_line(void) 
{ 
	dis_cmd(0xC0); 
	_delay_ms(1); 
} 

void lcd_1st_line(void) 
{ 
	dis_cmd(0x80); 
_	delay_ms(1); 
} 

void lcd_init() // fuction for intialize 
{ 
	DDRA=0xFF; 
	dis_cmd(0x02); // to initialize LCD in 4-bit mode. 
	dis_cmd(0x28); //to initialize LCD in 2 lines, 5X7 dots and 4bit mode. 
	dis_cmd(0x0C); 
	dis_cmd(0x06); 
	dis_cmd(0x80); 
	dis_cmd(0x01); 
	_delay_ms(10); 
} 

void dis_cmd(char cmd_value) 
{ 
	char cmd_value1; 
	cmd_value1 = cmd_value & 0xF0; //mask lower nibble because PA4-PA7 pins are used. 
	lcdcmd(cmd_value1); // send to LCD 
	cmd_value1 = ((cmd_value<<4) & 0xF0); //shift 4-bit and mask 
	lcdcmd(cmd_value1); // send to LCD 
}
 
void dis_data(char data_value) 
{ 
	char data_value1; 
	data_value1=data_value&0xF0; 
	lcddata(data_value1); 
	data_value1=((data_value<<4)&0xF0); 
	lcddata(data_value1); 
} 

void lcdcmd(char cmdout) 
{ 
	PORTA=cmdout; 
	PORTA&=~(1<<rs); 
	PORTA&=~(1<<rw); 
	PORTA|=(1<<en); 
	_delay_ms(1); 
	PORTA&=~(1<<en); 
} 

void lcddata(char dataout) 
{ 
	PORTA=dataout; 
	PORTA|=(1<<rs); 
	PORTA&=~(1<<rw); 
	PORTA|=(1<<en); 
	_delay_ms(1); 
	PORTA&=~(1<<en); 
} 
#endif