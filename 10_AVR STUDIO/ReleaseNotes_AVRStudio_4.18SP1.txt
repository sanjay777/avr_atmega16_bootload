Welcome to AVR Studio 4.18 Service Pack 1 (12/2009)

Part support
The following (5) new parts have been added to AVR Studio since 4.18:
-ATmega16HVA2, ATmega329PA, ATmega48A, ATmega88A, ATmega168A


New Features
AVR Dragon support for XMEGA PDI mode
AVRISP mkII support for TPI in programming dialog in AVR Studio
QT600 support for TPI in programming dialog in AVR Studio
Simulator2 model for: ATmega16HVA2

Bug Fixes
10869 - Command line utility AVRDragon fails for parallel programming of ATMega168
10847 - AVR ONE!: Incorrect help link in error dialog, programming dialog
10859 - ELF file will write all fuses to FF if fuses has not been specified
10737 - jtagiceii.exe with more than one debugger
10583 - jtagiceii.exe cac-file location is wrong     
10760 - AVRISPmkII: Select serialnumber shows ALL connected Atmel devices
10806 - Simulator2: Set next statement executes next instruction
10975 - AVRDragon: AVR Studio crashes when a wrong device is selected in "Select
Platform and device"

Known Issues
10854 - Vista issues opening COFF files in AVR Studio 4.18: See work-around for this
here 

Note
Windows 95 is no longer supported by AVR Studio. The most recent version that supported Windows 95 was AVR Studio 4.12 SP3.
Windows 98 is no longer supported by AVR Studio. The most recent version that supported Windows 98 was AVR Studio 4.16 SP1.

