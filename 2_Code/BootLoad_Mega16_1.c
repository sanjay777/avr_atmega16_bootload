//https://forum.pololu.com/t/super-simple-bootloader/525/2

#include <avr/boot.h>


/* Select Boot Size (select one, comment out the others) */
//#define _B128 
//#define _B256 
#define _B512 			// boot size is 512 words (1024 bytes)
//#define _B1024
#include "chipdef.h"


#define SERIAL_BAUD_RATE  10	// 115200 @ 20MHz: CLOCK/BAUD/16 - 1

#define BL_VERSION_MAJOR	'1'
#define BL_VERSION_MINOR	'0'


unsigned char checksum;


//_____________________________________________________________________________
//
// Serial
//_____________________________________________________________________________

#define	serialReceiveReady()		(UCSR0A & (1<<RXC0))
#define serialDataRegisterReady()	(UCSR0A & (1<<UDRE0))
#define	serialReceiveNow()			(UDR0)
#define	serialTransmitMacro(byte) 	while (!serialDataRegisterReady());	\
									UDR0 = byte


void serialTransmit(unsigned char byte) 
{
	while (!serialDataRegisterReady());	// Wait for empty transmit buffer
	UDR0 = byte;	// send byte
}


unsigned char serialReceive(void)
{
	while (!serialReceiveReady())
		;
	return UDR0;							// Get byte
}

unsigned char serialReceiveHexNibble(void)
{
	unsigned char in = serialReceive();
	if (in >= 'A')
		in -= 'A' - 10;
	else
		in -= '0';
	return in;
}

unsigned char bootloaderGetData(void)
{
	unsigned char in;

	in = serialReceiveHexNibble() << 4;
	in |= serialReceiveHexNibble();
	checksum += in;
	return in;
}


//_____________________________________________________________________________


void (*jumpToApp)(void) = 0x0000;


int main(void)
{	
	// Serial Init 
	UBRR0 = (unsigned int)SERIAL_BAUD_RATE;	// Set baud rate
	UCSR0C = (3 << UCSZ00);				// 1 stop bit, 8-bit characters
	UCSR0B = _BV(RXEN0) | _BV(TXEN0);	// enable UART receiver and transmitter


	// *** Do something here to decide whether to remain in the bootloader
	//      to leave the bootloader, call jumpToApp()


	top:				// jump back here if there's an error receiving upload

	// *** Require some sort of handshake before entering main bootloader code


	while (1)
	{   
		serialTransmit(':'); // Notify that we are in the bootloader code

		unsigned char in = serialReceive();

		if (in == 'u')
		{
			unsigned char memory[SPM_PAGESIZE]; // create temporary page
			unsigned int address;
			unsigned char count, type, i;
			unsigned int mem_data;

			type = 0;						
			address = 0; 					// erase application portion
			while (address < APP_END)
			{
				boot_page_erase(address);	// perform page erase
				boot_spm_busy_wait();		// wait until the memory is erased.
				address += SPM_PAGESIZE;
			}

			// echo capital letter back once application portion is erased
			serialTransmit('U');		

			do
			{
				if (serialReceive() != ':')	// get the packet start
				{
					serialTransmit( 'e' );		// start byte error
					goto top;
				}
	
				checksum = 0;			
				count = bootloaderGetData();	// get the packet size
				address = bootloaderGetData();	// get the memory address
				address <<= 8;
				address |= bootloaderGetData();
				type = bootloaderGetData();		// get the packet type

				for (i = 0; i < count; i++)		// get the data
					memory[i] = bootloaderGetData();

				bootloaderGetData();			// get the checksum
	
				if (checksum)					// check the checksum
				{
					serialTransmit( 'c' );		// checksum error
					goto top;
				}
					
				serialReceive();				// get the LF
	
				if (type == 0) 
				{
					while (count < SPM_PAGESIZE)	// fill rest of buffer
						memory[count++] = 0xFF;
		
					for(i = 0; i < SPM_PAGESIZE;)	// store in temp buffer
					{
						mem_data = memory[i++];
						mem_data |= (memory[i++] << 8);
						boot_page_fill(address, mem_data); 
						address += 2; 
					}
		
					boot_page_write(address-SPM_PAGESIZE);	// write to flash
					boot_spm_busy_wait();	
					boot_rww_enable();			// we-enable the RWW section
				}
				serialTransmit('.');			// signify valid packet
			} while(type != 1);

			if (type == 1)
			{
				serialTransmit('*');			// signify valid upload
				jumpToApp();					// jump to the start of flash
			}

		}	// end if (in == 'u')
		else if (in == 'x')
		{
			serialTransmit('X');
			jumpToApp();
		}
		else if (in == 'v')
		{
			serialTransmit(BL_VERSION_MAJOR);
			serialTransmit('.');
			serialTransmit(BL_VERSION_MINOR);
		}

    }	// end while(1)
	
	
    return 0;
}